// error.h
// �e�en� IJC-DU1, p��klad a), 11.03.2015
// Autor: Karel Pokorn�, FIT
// P�elo�eno: gcc 4.8

// Define guard
#ifndef ERROR_H
#define ERROR_H

#include <stdio.h>
#include <stdarg.h>
#include <stdlib.h>

void Warning(const char *fmt, ...);
void FatalError(const char *fmt, ...);

#endif // ERROR_H_INCLUDED