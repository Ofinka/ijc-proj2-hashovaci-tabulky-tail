// error.c
// �e�en� IJC-DU1, p��klad a), 11.03.2015
// Autor: Karel Pokorn�, FIT
// P�elo�eno: gcc 4.8

#include "error.h"

//Tisk chyby na stderr
//Prijima: formatovaci retezec (const char *) ve stejnem formatu jako prijima
//         napr. funkce printf a dale prijima variabilni pocet parametru
void Warning(const char *fmt, ...)
{
    va_list arg;                    // Pointer na promenne arg
    va_start(arg, fmt);             // Nastavit pointer na fmt
    fprintf(stderr, "CHYBA: ");     // Vytisknout opakujici se cast
    vfprintf(stderr, fmt, arg);     // Vytisknout text 
    fprintf(stderr, "\n");
    va_end(arg);                    // Uvolneni pointeru
    
    exit(2);
}

//Tisk chyby na stderr s naslednym ukoncenim programu (navratova hodnota 1)
//Prijima: formatovaci retezec (const char *) ve stejnem formatu jako prijima
//         napr. funkce printf a dale prijima variabilni pocet parametru
void FatalError(const char *fmt, ...)
{
    va_list arg;                    // Pointer na promenne arg
    va_start(arg, fmt);             // Nastavit pointer na fmt
    fprintf(stderr, "CHYBA: ");     // Vytisknout opakujici se cast
    vfprintf(stderr, fmt, arg);     // Vytisknout text
    fprintf(stderr, "\n");
    va_end(arg);                    // Uvolneni pointeru

    exit(1);
}