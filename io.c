// io.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "io.h"


// nacte slovo oddelene bilym znakem a vraci pocet pismen
int fgetword(char *s, int max, FILE *f)
{
    if (!f || !s)
        return EOF; 

    int c;
    
    // preskocime uvodni bile znaky a zastavime se u prvniho nebileho
    for (; (c = fgetc(f)) != EOF && isspace(c); ) ;
    
    if (c == EOF)
        return EOF;
    
    // zapiseme prvni znak
    *s = c;

    int i;
    for (i = 1; isspace((c = fgetc(f))) == 0; i++)
    {
        // pripojime \0
        if (c == EOF)
        {
            s[i] = '\0';
            return EOF;
        }
        // prekrocili sme limit cteni
        else if (i + 1 >= max)
        {
            for (; !isspace(getc(f));)
                fprintf(stderr, "prilis dlouhe slovo -> orezavam\n");
            break; 
        }
        s[i] = c;
    }
    // pripojime konc. nulu
    s[i] = '\0'; 

    return i;
}