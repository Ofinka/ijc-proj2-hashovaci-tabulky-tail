// htab_clear.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"

// zruseni vsech polozek v tabulce
void htab_clear(htab_t *table)
{
    // kdyz neexistuji
    if (!table || !table->data)    
        return;

    struct htab_listitem *tmp = NULL;

    unsigned int i;
    for (i = 0; i < table->size; i++)
    {
        // pokracujeme kdyz radek neni obsazeny
        if (table->data[i] == NULL)   
            continue;

        // nactu prvni prvek
        tmp = table->data[i];      
        while (tmp != NULL)             
        {
            if (tmp->key != NULL)
                free(tmp->key);     
            
            // vytvorim si pomocny ukazatel
            struct htab_listitem* dalsi = tmp->next;  
            
            if (tmp != NULL)
                free(tmp);      
            tmp = dalsi;     
        }
        // zahodime ukazatel na radek
        table->data[i] = NULL;    
    }
}
