// tail.c
// �e�en� IJC-DU2, p��klad a), 12.04.2015
// Autor: Karel Pokorn�, FIT


#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include <ctype.h>
#include "error.h"

#define MAX_BUFF_SIZE 510 
bool plus = false;

typedef struct params
{
    char* path;
    int rows;
    bool isFile;
} params;

bool isStringNumber(char* str)
{
    for(int i = 0; str[i] != 0; i++)
    {
        // nastav plus pro rozeznani parametru
        if(str[i] == '+')
        {   
            plus = true;
            continue;
        }
        // udelam absolutni hodnotu z poctu radku -> jako dela POSIX tail
        if(str[i] == '-')
        {
            str[i] = '+';
            continue;
        }
        if(!isdigit(str[i]))
            return false;
    }
    return true;
}

// priradim hodnoty a parametry do struktury
params getParams(int argc, char** argv)
{
    params pars;
    pars.rows = 10;
    pars.isFile = true;
    
    if (argc > 4)
        FatalError("Chybny pocet argumentu");
   
    // priradim informace o -n
    if (argc > 2)
    {
        if (strcmp("-n", argv[1]) == 0)
        {
            if (isStringNumber(argv[2]))
                pars.rows = (atoi(argv[2]));
            else
                FatalError("Pocet radku musi byt cele cislo");      
        }
    }

    // kdyz se jedna o moznost: ./tail "jmeno_souboru"
    if ((argc == 2) && (strcmp("-n", argv[1]) != 0))
        pars.path = argv[1];
    // kdyz se jedna o moznost: ./tail -n X "jmeno_souboru"
    else if ((argc == 4) && (strcmp("-n", argv[1]) == 0))
        pars.path = argv[3];
    // kdyz se jedna o moznost: ./tail stdin    ||    ./tail -n X stdin
    else if (argc == 1 || argc == 3)
        pars.isFile = false;
    else
        FatalError("Spatny format argumentu");
        
    return pars;
}

// zkontroluj a otevri soubor
FILE *open(const char *path)
{
    FILE *file;
    file= fopen(path, "r");

    if(file == NULL)
        FatalError("Chyba pri otevirani souboru \"%s\"", path);
    return(file);
}

void printLines(FILE* file, params pars)
{
    char buff[pars.rows][MAX_BUFF_SIZE]; // bude potreba buffer na nacitani jednotlivych radku
    int c;
    bool reported = false;
    
    if (plus) // tiskneme od zadaneho radku do konce
    {
        for (long i = 1; ; i++)    // prvni radek ma cislo 1
        {
            // konec souboru
            if (fgets(buff[0], MAX_BUFF_SIZE, file) == NULL)
                break;
            // v pripade ze je radek delsi nez MAX_BUFF_SIZE a uz byl reportnuty
            if ((strchr(buff[0], '\n') == NULL) && (reported))
            {
                buff[0][MAX_BUFF_SIZE-1] = '\n'; 
                while ((c = getc(file)) != '\n') ;  // preskocim zbytek radku,
            }
            // narazil jsem na prilis dlouhy radek
            if ((strchr(buff[0], '\n') == NULL) && !reported)
            {
                fprintf(stderr, "PRILIS DLOUHY RADEK\n");
                reported = true;
                buff[0][MAX_BUFF_SIZE-1] = '\n'; 
                while ((c = getc(file)) != '\n') ;  // preskocim zbytek radku
            }
            // tiskneme radek
            if (i >= pars.rows)
                printf("%s", buff[0]);     
        }
    }
    else
    {
        long i;
        for (i = 0; ; i++)
        {
            // konec souboru
            if (fgets(buff[i % pars.rows], MAX_BUFF_SIZE, file) == NULL)
                break;
            // v pripade ze je radek delsi nez MAX_BUFF_SIZE a uz byl reportnuty
            if (strchr(buff[i % pars.rows], '\n') == NULL && reported)
            {
                buff[i % pars.rows][MAX_BUFF_SIZE-1] = '\n'; 
                while ((c = getc(file)) != '\n') ;  // preskocim zbytek radku
            }
            // narazil jsem na prilis dlouhy radek
            if (strchr(buff[i % pars.rows], '\n') == NULL && !reported)
            {
                reported = true;
                fprintf(stderr, "PRILIS DLOUHY RADEK\n");
                buff[i % pars.rows][MAX_BUFF_SIZE-1] = '\n'; 
                while ((c = getc(file)) != '\n') ;  // preskocim zbytek radku
            }
        }
        // kdyz bude zadano vice radku nez obsahuje soubor
        if (pars.rows > i)
            pars.rows = i;
        // vytiskneme radky
        for (long j = 0 ; j < pars.rows; i++, j++)     
            printf("%s", buff[i % pars.rows]);
    }
}

int main(int argc, char** argv)
{
    params pars = getParams(argc, argv);   
    // kdyz bude zadano 0 radku
    if (pars.rows == 0)
        return 0;
    
    FILE* file;
    // nastavime zdroj a posleme do funkce ktera vytiskne pozadovane radky
    if (pars.isFile)
    {
        file = open(pars.path);
        printLines(file, pars);  
        fclose(file);
    }
    else // pokud neni zadan soubor
    {
        file = stdin;
        printLines(file, pars);
    }
    
     
        
    return 0;
}