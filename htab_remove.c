// htab_remove.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"

// najde a odstrani prvek se stejnym klicem
void htab_remove(htab_t *t, const char *key)
{
    if (!t || !key)
        return;

    unsigned int index = hash_function(key, t->size);

    struct htab_listitem *item = t->data[index];
    struct htab_listitem *prevItem = NULL;

    // prochazime cely radek tabulky
    while (item != NULL)   
    {
        if (strcmp(item->key, key) == 0)    
        {
            if (item->next != NULL && prevItem == NULL ) 
                t->data[index] = item->next;
            else if (item->next != NULL && prevItem != NULL)
                prevItem->next = item->next;
            else if (prevItem == NULL && item->next == NULL) 
                t->data[index] = NULL; 
            else
                prevItem->next = NULL;  

            free(item->key);   
            item->next = NULL;
            free(item);
            break;     
        }
        prevItem = item;
        item = item->next;
    }
}
