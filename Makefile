# Makefile
# �e�en� IJC-DU2, p��klad a), 12.04.2015
# Autor: Karel Pokorn�, FIT

CFLAGS=-std=c99 -pedantic -Wall -Wextra 
CCFLAGS=-std=c++11 -pedantic -Wall
CC=gcc

all: tail tail2 wordcount wordcount-dynamic

###############################################################
###############################################################
tail: tail.o error.o
	$(CC) $(CFLAGS) tail.o error.o -o tail
    
tail2: tail2.o
	g++ $(CCFLAGS) tail2.o -o tail2
    
wordcount: wordcount.o libhtable.a io.o
	$(CC) $(CFLAGS) -o wordcount -static io.o wordcount.o libhtable.a 
    
wordcount-dynamic: wordcount.o io.o libhtable.so
	$(CC) $(CFLAGS) -o wordcount-dynamic wordcount.o io.o libhtable.so
    
###############################################################
# OBJECTIVE FILES
# -fPIC potreba pro dynamickou knihovnu
###############################################################
tail.o: tail.c error.h
	$(CC) $(CFLAGS) -c tail.c -o tail.o
    
error.o: error.c error.h
	$(CC) $(CFLAGS) -c error.c -o error.o

tail2.o: tail2.cc
	g++ $(CCFLAGS) -c tail2.cc -o tail2.o

wordcount.o: wordcount.c htable.h
	$(CC) $(FLAGS) -c wordcount.c -o wordcount.o
    
io.o:
	$(CC) $(CFLAGS) -c io.c -o io.o
    
hash_function.o: hash_function.c hash_function.h
	$(CC) $(CFLAGS) -fPIC -c hash_function.c
    
htab_init.o: htab_init.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_init.c -o htab_init.o

htab_lookup.o: htab_lookup.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_lookup.c -o htab_lookup.o
    
htab_foreach.o: htab_foreach.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_foreach.c -o htab_foreach.o 
    
htab_remove.o: htab_remove.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_remove.c -o htab_remove.o  
    
htab_clear.o: htab_clear.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_clear.c -o htab_clear.o
    
htab_free.o: htab_free.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_free.c -o htab_free.o
    
htab_statistics.o: htab_statistics.c htable.h
	$(CC) $(CFLAGS) -fPIC -c htab_statistics.c -o htab_statistics.o  

###############################################################    
# LIB FILES
#
###############################################################
# static
libhtable.a: htab_clear.o htab_init.o htab_lookup.o htab_free.o htab_foreach.o htab_remove.o hash_function.o htab_statistics.o
	ar crs libhtable.a htab_clear.o htab_init.o htab_lookup.o htab_free.o htab_foreach.o htab_remove.o hash_function.o htab_statistics.o
    
# dynamic    
libhtable.so: htab_clear.o htab_init.o htab_lookup.o htab_free.o htab_foreach.o htab_remove.o hash_function.o htab_statistics.o
	$(CC) $(CFLAGS) -shared -fPIC -o libhtable.so htab_clear.o htab_init.o htab_lookup.o htab_free.o htab_foreach.o htab_remove.o hash_function.o htab_statistics.o
    
###############################################################
# CISTENI
###############################################################   
clean:
	rm -f *.o
	rm -f tail
	rm -f tail2
	rm -f wordcount
	rm -f wordcount-dynamic
	rm -f libhtable.a libhtable.so
    