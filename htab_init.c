// htab_init.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"

// inicializace tabulky
htab_t *htab_init(unsigned int size)
{
    // size musi byt alespon 1
    if (size <= 0)
        return NULL;
    
    // alokace struktury -> flexible array member 
    // pokud se nepodari alokace
    htab_t *table = (htab_t*) malloc(sizeof(htab_t) + (size * sizeof(struct htab_listitem*)));
    // kdyz nastane chyba pri alokaci
    if (table == NULL)
        return NULL;
    
    // zapiseme
    table->size = size; 
    
    // vynulujeme
    unsigned int i;
    for (i = 0; i < size; i++)
        table->data[i] = NULL;
    
    return table;
}
