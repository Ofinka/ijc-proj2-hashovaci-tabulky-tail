// htab_foreach.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"


// provede stejnou operaci nad kazdym prvekm
void htab_foreach(htab_t *t, void (*function)(const char*, unsigned int))
{
    if (!t)
        return;

    struct htab_listitem *item = NULL;

    unsigned int i;
    for (i = 0; i < t->size; i++)   
    {
        item = t->data[i];

        while (item != NULL)   
        {
            function(item->key, item->data);    
            item = item->next;
        }
    }
}
