// htab_free.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"

void htab_free(htab_t *table)
{
    if (!table)
        return;
    
    htab_clear(table);  //smazeme (uvolnime) vsechny polozky v tabulce
    free(table);  //uvolnime celou tabulku
    table = NULL; //zahodime ukazatel
}
