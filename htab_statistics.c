// htab_statistics.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"

#define WORD_LIM 127

void htab_statistics(htab_t* table)
{
    if (!table)
        return;

    struct htab_listitem* item = NULL;

    int min = WORD_LIM;
    int max, counter = 0;
    int sum = 0;

    for (unsigned int i = 0; i < table->size; i++)
    {
        item = table->data[i];

        for (; item != NULL; )
        {
            if (item->data < min)
                min = item->data;
            if (item->data > max)
                max = item->data;

            sum += item->data;
            counter++;

            item = item->next;
        }
    }

    double avg = (double)sum/(double)counter;
    printf("minimalni delka seznamu je %u\n", min);
    printf("maximalni delka seznamu je %u\n", max);
    printf("prumerna delka seznamu je %f\n", avg);
}