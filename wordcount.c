// wordcount.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"
#include "io.h"

#define WORD_LIM 127
#define TAB_SIZE 24578

// vytiskne obsah polozky
void printWords(const char *key, unsigned int value)
{
    printf("%s\t%u\n", key, value);
}

int main(void)
{
    htab_t *table = htab_init(TAB_SIZE);
    if (table == NULL)
    {
        fprintf(stderr, "Malo pameti pro alokaci hash tabulky\n");
        return EXIT_FAILURE;
    }
    
    // slovo + \0
    char word[WORD_LIM + 1] = {'\0'};       
    while (fgetword(word, WORD_LIM, stdin) != EOF)  
    {
        // pokud se nepovede pridat polozku
        if (htab_lookup(table, word) == NULL)    
        {
            fprintf(stderr, "Malo pameti pro zaznam\n");
            htab_free(table);
            return EXIT_FAILURE;
        }
    }
    // vytiskneme vsechyn polozky a pocet jejich vyskytu
    htab_foreach(table, printWords);      
    
    htab_statistics(table);
    
    // uvolnime tabulku
    htab_free(table);   

    return EXIT_SUCCESS;
}