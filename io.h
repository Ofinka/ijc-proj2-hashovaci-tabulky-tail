// io.h
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#ifndef IO_H_INCLUDED
#define IO_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>

int fgetw(char *s, int max, FILE *f);

#endif  
