// htab_lookup.c
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#include "htable.h"


// v tabulce t vyhleda zaznam odpovidajici key
struct htab_listitem *htab_lookup(htab_t *t, const char *key)
{
    if (!t || !key)
        return NULL;
    
    unsigned int index = hash_function(key, t->size);    

    struct htab_listitem *item = t->data[index];   
    // nemuze existovat predchozi prvek kdyz zaciname od zacatku    
    struct htab_listitem *prevItem = NULL; 

    // podivame se jestli je nas prvek v tabulce
    while (item != NULL)    
    {
        // kdyz najdeme prvek jehoz klic se shoduje
        if (strcmp(key, item->key) == 0)    
        {
            item->data += 1;    
            return item;        
        }
        prevItem = item;
        item = item->next;
    }

    // kdyz prvek v tabulce neni musime ho pridat
    struct htab_listitem *addItem = malloc(sizeof(*addItem));
    
    if (addItem == NULL)
        return NULL;

    addItem->key = (char*) malloc(strlen(key) + 1);     
    
    if (addItem->key == NULL)
    {
        free(addItem);
        return NULL;
    }

    strcpy(addItem->key, key);      
    addItem->data = 1;             
    addItem->next = NULL;

    if (prevItem != NULL)     
        prevItem->next = addItem;
    else
        t->data[index] = addItem;   

    return addItem;
}
