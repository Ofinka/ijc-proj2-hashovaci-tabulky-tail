// htable.h
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#ifndef HTABLE_H_INCLUDED
#define HTABLE_H_INCLUDED

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "hash_function.h"

// polozka v hash. tabulce
struct htab_listitem
{
    char *key;                  // ukazatel na dynamicky alokovany retezec
    int data;                   // pocet vyskytu 
    struct htab_listitem *next; // ukazatel na dalsi polozku
};

// hashovaci tabulka
typedef struct htab
{
    unsigned int size;                 //velikost pole ukazatelu
    struct htab_listitem *data[];      //pole ukazatelu
} htab_t;

// inicializace a vytvoreni tabulky
htab_t *htab_init(unsigned int size);

// v tabulce t vyhleda zaznam odpovidajici key
// kdyz najde preda ukazatel na zaznam
// kdyz nenajde, vytvori zaznam a preda ukazatel
struct htab_listitem *htab_lookup(htab_t *t, const char *key);

// vola zadanou funkci pro kazdy prvek tabulky, jejiz obsah nezmeni -> const
void htab_foreach(htab_t *t, void (*function)(const char *, unsigned int));

// vyhleda a zrusi zadanou polozku
void htab_remove(htab_t *t, const char *key);

// zruseni vsech polozek v tabulce
void htab_clear(htab_t *t);

// zruseni cele tabulky
void htab_free(htab_t *t);

// tiskne prumerne a min/max. delky seznamu v tabulce
void htab_statistics(htab_t *t);

#endif // htab_H_INCLUDED