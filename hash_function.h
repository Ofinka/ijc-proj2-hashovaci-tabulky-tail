// hash_function.h
// Řešení IJC-DU2, příklad a), 12.04.2015
// Autor: Karel Pokorný, FIT

#ifndef HASH_FUNCTION_H_INCLUDED
#define HASH_FUNCTION_H_INCLUDED

#include <stdio.h>
#include <stdlib.h>

unsigned int hash_function(const char *str, unsigned htab_size);

#endif